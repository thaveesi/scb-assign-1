package com.example.scbassign1.user;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {
    @Mock
    private UserRepository userRepository;
    private UserService underTest;

    @BeforeEach
    void setup() {
       underTest = new UserService(userRepository);
    }



    @Test
    void canGetUsers() {
        //When
        underTest.getUsers();
        //then
        verify(userRepository).findAll();
    }

    @Test
    void canAddNewUser() {
        //given
        User user = new User(
                "peter",
                "peter@gmail.com",
                LocalDate.of(2000,1,28),
                19
        );
        //when
        underTest.addNewUser(user);

        //then
        ArgumentCaptor<User> userArgumentCaptor = ArgumentCaptor.forClass(User.class);

        verify(userRepository).save(userArgumentCaptor.capture());

        User capturedUser =userArgumentCaptor.getValue();

        assertThat(capturedUser).isEqualTo(user);
    }
}