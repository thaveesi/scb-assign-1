package com.example.scbassign1.user;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDate;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class UserRepositoryTest {

    @Autowired
    private UserRepository underTest;

    @AfterEach
    void tearDown(){
        underTest.deleteAll();
    }

    @Test
    void shouldFindUserByEmail() {
        //given
        Boolean real = false;
        String email= "peter@gmail.com";
        User user = new User(
                "peter",
                email,
                LocalDate.of(2000,1,28),
                19
        );
        underTest.save(user);
        //when
        Optional<User> exists = underTest.findUserByEmail(email);
        if (exists.isPresent())
        {
            real = true;
        }
        //then
        assertThat(real).isTrue();
    }
}