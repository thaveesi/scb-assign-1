package com.example.scbassign1.user;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.util.List;

@Configuration
public class UserConfig {

    @Bean
    CommandLineRunner commandLineRunner(UserRepository repository){
        return args -> {
            User pete = new User(
                    "Pete",
                    "thaveesi@usc.edu",
                    LocalDate.of(2001,1,11),
                    21
                    );

            User pan = new User(
                    "Pan",
                    "pani@gmail.com",
                    LocalDate.of(2002,1,14),
                    19
            );

            repository.saveAll(
                    List.of(pete, pan)
            );
        };
    }
}
