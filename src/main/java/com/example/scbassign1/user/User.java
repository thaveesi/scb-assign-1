package com.example.scbassign1.user;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "user_table")
public class User {
    @Id
    @SequenceGenerator(
        name = "user_sequence",
        sequenceName = "user_sequence",
        allocationSize = 1
    )

    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "user_sequence"
    )

//    @Getter @Setter
    private Long ID;
//    @Getter @Setter
    private String name;
//    @Getter @Setter
    private String email;
//    @Getter @Setter
    private LocalDate dob;
//    @Getter @Setter
    private Integer age;

    public User() {
    }

    public User(Long ID, String name, String email, LocalDate dob, Integer age) {
        this.ID = ID;
        this.name = name;
        this.email = email;
        this.dob = dob;
        this.age = age;
    }

    public User(String name, String email, LocalDate dob, Integer age) {
        this.name = name;
        this.email = email;
        this.dob = dob;
        this.age = age;
    }

    @Override
    public String toString() {
        return "User{" +
                "ID=" + ID +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", dob=" + dob +
                ", age=" + age +
                '}';
    }
}
