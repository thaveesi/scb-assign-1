package com.example.scbassign1;

import com.example.scbassign1.user.User;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
//import java.time.LocalDate;
//import java.time.Month;
//import java.util.*;

@SpringBootApplication
@RestController
public class ScbAssign1Application {

	public static void main(String[] args) {
		SpringApplication.run(ScbAssign1Application.class, args);
	}


}
