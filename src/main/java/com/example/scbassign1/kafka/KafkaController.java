package com.example.scbassign1.kafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class KafkaController {

    @Autowired
    Producer producer;

    @PostMapping(value="/post")
    public void sendMessage(@RequestParam("msg") String msg) {
        producer.publishTopic(msg);
    }
}
