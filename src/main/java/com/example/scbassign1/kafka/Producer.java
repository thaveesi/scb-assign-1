package com.example.scbassign1.kafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.core.KafkaTemplate;
import com.example.scbassign1.kafka.Producer;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class Producer {
    public static final String topic = "mytopic";

    @Autowired
    private KafkaTemplate<String, String> kafkaTemp;

    public void publishTopic(String message) {
        System.out.println("Publishing to topic "+topic);
        this.kafkaTemp.send(topic,message);
    }
}